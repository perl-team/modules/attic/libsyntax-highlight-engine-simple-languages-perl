#!/bin/sh
PERL=$(which perl)
MAKE=$(which make)

if [ -d $1 ]; 
then
    echo "=========================================================================="
    echo "Clean $1"
    echo "=========================================================================="
else
    echo "directory $1 does not exist"
    exit 1
fi
cd $1
if [ -r Build.PL ];
then
    [ ! -f Build ] || ${PERL} Build --allow_mb_mismatch 1 distclean
else
    [ ! -f Makefile ] || ${MAKE} realclean
fi
