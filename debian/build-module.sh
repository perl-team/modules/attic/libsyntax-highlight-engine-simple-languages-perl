#!/bin/sh

PERL=$(which perl)
MAKE=$(which make)
PERL5LIB="$DESTDIR/usr/share/perl5/"
PERL="$PERL -I$PERL5LIB"

if [ -d $1 ]; 
then
    echo "=========================================================================="
    echo "Make $1"
    echo "PERL5LIB = $PERL5LIB"
    echo "DESTDIR = $DESTDIR"
    echo "=========================================================================="
else
    echo "directory $1 does not exist"
    exit 1
fi
cd $1
if [ -r Build.PL ];
then
    $PERL Build.PL installdirs=vendor
    $PERL Build
    $PERL Build test
    $PERL Build install destdir=$DESTDIR
else
    PERL5LIB=$PERL5LIB $PERL Makefile.PL INSTALLDIRS=vendor --skipdeps
    PERL5LIB=$PERL5LIB $MAKE
    PERL5LIB=$PERL5LIB $MAKE test
    $MAKE install DESTDIR=$DESTDIR
fi
